# -*- coding: utf-8 -*-
import unittest
import os  # noqa: F401
import json  # noqa: F401
import time
import shutil
import zipfile
import sys
from metabat.Utils.MetabatUtil import MetabatUtil

from os import environ
try:
    from ConfigParser import ConfigParser  # py2
except:
    from configparser import ConfigParser  # py3

from pprint import pprint  # noqa: F401

from biokbase.workspace.client import Workspace as workspaceService
from metabat.metabatImpl import metabat
from metabat.metabatServer import MethodContext
from metabat.authclient import KBaseAuth as _KBaseAuth
from ReadsUtils.ReadsUtilsClient import ReadsUtils
from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from metabat.Utils.combineDepthTablesUtil import CombineDepthTables


class metabatTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.token = environ.get('KB_AUTH_TOKEN', None)
        config_file = environ.get('KB_DEPLOYMENT_CONFIG', None)
        cls.cfg = {}
        config = ConfigParser()
        config.read(config_file)
        for nameval in config.items('metabat'):
            cls.cfg[nameval[0]] = nameval[1]
        # Getting username from Auth profile for token
        authServiceUrl = cls.cfg['auth-service-url']
        auth_client = _KBaseAuth(authServiceUrl)
        user_id = auth_client.get_user(cls.token)
        # WARNING: don't call any logging methods on the context object,
        # it'll result in a NoneType error
        cls.ctx = MethodContext(None)
        cls.ctx.update({'token': cls.token,
                        'user_id': user_id,
                        'provenance': [
                            {'service': 'metabat',
                             'method': 'please_never_use_it_in_production',
                             'method_params': []
                             }],
                        'authenticated': 1})
        cls.wsURL = cls.cfg['workspace-url']
        cls.wsClient = workspaceService(cls.wsURL)
        cls.serviceImpl = metabat(cls.cfg)
        cls.scratch = cls.cfg['scratch']
        cls.callback_url = os.environ['SDK_CALLBACK_URL']
        suffix = int(time.time() * 1000)
        cls.wsName = "test_metabat_" + str(suffix)
        cls.ws_info = cls.wsClient.create_workspace({'workspace': cls.wsName})
        
        cls.ru = ReadsUtils(os.environ['SDK_CALLBACK_URL'], token=cls.token)
        cls.au = AssemblyUtil(os.environ['SDK_CALLBACK_URL'], token=cls.token)
        cls.prepare_data()

    @classmethod
    def tearDownClass(cls):
        if hasattr(cls, 'wsName'):
            cls.wsClient.delete_workspace({'workspace': cls.wsName})
            print('Test Workspace was Deleted')
    
    @classmethod
    def prepare_data(cls):
        """
        Lets put everything on workspace
        """  
        # upload some depth files for merge testing
        cls.depth_file_list=[]
        for d in range(1,2):
            depth = str(d) + '.depth.json'
            cls.depth_scratch = os.path.join(cls.scratch, depth)
            shutil.copy(os.path.join("data", depth), cls.depth_scratch)
            cls.depth_file_list.append(cls.depth_scratch)

        #
        # READS 1
        # building Interleaved library
        pe1_reads_filename = 'lib1.oldstyle.fastq'
        pe1_reads_path = os.path.join(cls.scratch, pe1_reads_filename)
        
        # gets put on scratch. "work/tmp" is scratch        
        shutil.copy(os.path.join("data", "reads_file", pe1_reads_filename), pe1_reads_path)
        
        int1_reads_params = {
            'fwd_file': pe1_reads_path,
            'sequencing_tech': 'Unknown',
            'wsname': cls.ws_info[1],
            'name': 'MyInterleavedLibrary1',
            'interleaved': 'true'
        }
        
        #from scratch upload to workspace
        cls.int1_oldstyle_reads_ref = cls.ru.upload_reads(int1_reads_params)['obj_ref']                    
        
        # READS 2
        # building Interleaved library
        pe2_reads_filename = 'lib2.oldstyle.fastq'
        pe2_reads_path = os.path.join(cls.scratch, pe2_reads_filename)
        
        # gets put on scratch. "work/tmp" is scratch        
        shutil.copy(os.path.join("data", "reads_file", pe2_reads_filename), pe2_reads_path)
        
        int2_reads_params = {
            'fwd_file': pe2_reads_path,
            'sequencing_tech': 'Unknown',
            'wsname': cls.ws_info[1],
            'name': 'MyInterleavedLibrary2',
            'interleaved': 'true'
        }
                
        #from scratch upload to workspace
        cls.int2_oldstyle_reads_ref = cls.ru.upload_reads(int2_reads_params)['obj_ref']
        
        # # READS 3
        # # building Interleaved library
        # pe1_reads_filename = 'B12.W.fq'
        # pe1_reads_path = os.path.join(cls.scratch, pe1_reads_filename)
        # 
        # # gets put on scratch. "work/tmp" is scratch        
        # shutil.copy(os.path.join("data", "reads_file", pe1_reads_filename), pe1_reads_path)
        # 
        # int3_reads_params = {
        #     'fwd_file': pe1_reads_path,
        #     'sequencing_tech': 'Unknown',
        #     'wsname': cls.ws_info[1],
        #     'name': 'MyInterleavedLibrary1',
        #     'interleaved': 'true'
        # }
        # 
        # #from scratch upload to workspace
        # cls.int3_oldstyle_reads_ref = cls.ru.upload_reads(int3_reads_params)['obj_ref']
        # 
        # # READS 4
        # # building Interleaved library
        # pe1_reads_filename = 'E21.fq'
        # pe1_reads_path = os.path.join(cls.scratch, pe1_reads_filename)
        # 
        # # gets put on scratch. "work/tmp" is scratch        
        # shutil.copy(os.path.join("data", "reads_file", pe1_reads_filename), pe1_reads_path)
        # 
        # int4_reads_params = {
        #     'fwd_file': pe1_reads_path,
        #     'sequencing_tech': 'Unknown',
        #     'wsname': cls.ws_info[1],
        #     'name': 'MyInterleavedLibrary1',
        #     'interleaved': 'true'
        # }
        # 
        # #from scratch upload to workspace
        # cls.int4_oldstyle_reads_ref = cls.ru.upload_reads(int4_reads_params)['obj_ref']
        # 
        # # READS 5
        # # building Interleaved library
        # pe1_reads_filename = 'R11.fq'
        # pe1_reads_path = os.path.join(cls.scratch, pe1_reads_filename)
        # 
        # # gets put on scratch. "work/tmp" is scratch        
        # shutil.copy(os.path.join("data", "reads_file", pe1_reads_filename), pe1_reads_path)
        # 
        # int5_reads_params = {
        #     'fwd_file': pe1_reads_path,
        #     'sequencing_tech': 'Unknown',
        #     'wsname': cls.ws_info[1],
        #     'name': 'MyInterleavedLibrary1',
        #     'interleaved': 'true'
        # }
        # 
        # #from scratch upload to workspace
        # cls.int5_oldstyle_reads_ref = cls.ru.upload_reads(int5_reads_params)['obj_ref']
        
        #
        # building Assembly
        #
        assembly_filename = 'small_arctic_assembly.fa'
        cls.assembly_filename_path = os.path.join(cls.scratch, assembly_filename)
        shutil.copy(os.path.join("data", assembly_filename), cls.assembly_filename_path)
        
        # from scratch upload to workspace
        assembly_params = {
            'file': {'path': cls.assembly_filename_path},
            'workspace_name': cls.ws_info[1],
            'assembly_name': 'MyAssembly'
        }
        
        # puts assembly object onto shock
        cls.assembly_ref = cls.au.save_assembly_from_fasta(assembly_params)
        
    def getWsClient(self):
        return self.__class__.wsClient
    
    def getWsName(self):
        return self.ws_info[1]

    def getImpl(self):
        return self.__class__.serviceImpl

    def getContext(self):
        return self.__class__.ctx

    def test_merging_of_depth_files(self):
        self.combine = CombineDepthTables()
        final_depth_file = self.combine.createFinalDepthfile(self.scratch, self.depth_file_list, self.assembly_filename_path)
        num_lines = sum(1 for line in open(final_depth_file))
        self.assertEqual(num_lines,250)
        
    def test_bad_run_metabat_params(self):
        invalidate_input_params = {
          'missing_assembly_ref': 'assembly_ref',
          'binned_contig_name': 'binned_contig_name',
          'workspace_name': 'workspace_name',
          'reads_list': 'reads_list'
        }
        with self.assertRaisesRegexp(
                    ValueError, '"assembly_ref" parameter is required, but missing'):
            self.getImpl().run_metabat(self.getContext(), invalidate_input_params)
    
        invalidate_input_params = {
          'assembly_ref': 'assembly_ref',
          'missing_binned_contig_name': 'binned_contig_name',
          'workspace_name': 'workspace_name',
          'reads_list': 'reads_list'
        }
        with self.assertRaisesRegexp(
                    ValueError, '"binned_contig_name" parameter is required, but missing'):
            self.getImpl().run_metabat(self.getContext(), invalidate_input_params)
        
        invalidate_input_params = {
          'assembly_ref': 'assembly_ref',
          'binned_contig_name': 'binned_contig_name',
          'missing_workspace_name': 'workspace_name',
          'reads_list': 'reads_list'
        }
        with self.assertRaisesRegexp(
                    ValueError, '"workspace_name" parameter is required, but missing'):
            self.getImpl().run_metabat(self.getContext(), invalidate_input_params)
        
        invalidate_input_params = {
          'assembly_ref': 'assembly_ref',
          'binned_contig_name': 'binned_contig_name',
          'workspace_name': 'workspace_name',
          'missing_reads_list': 'reads_list'
        }
        with self.assertRaisesRegexp(
                    ValueError, '"reads_list" parameter is required, but missing'):
            self.getImpl().run_metabat(self.getContext(), invalidate_input_params)
    
    def test_pe_metabat_oldstyle(self):       
        # metabat should run to completion here
        ret = self.getImpl().run_metabat(self.getContext(),
                                            {'workspace_name': self.getWsName(),
                                             'assembly_ref': self.assembly_ref,
                                             'min_contig_length': 2000,
                                             'binned_contig_name': 'metabat_bins_obj',
                                             'reads_list': [self.int1_oldstyle_reads_ref, self.int2_oldstyle_reads_ref] }) #, self.int3_oldstyle_reads_ref, self.int4_oldstyle_reads_ref, self.int5_oldstyle_reads_ref]
                                             # })
